package main.java.spells.abstractions;

public enum DamageType {
    Physical,
    Magic
}
