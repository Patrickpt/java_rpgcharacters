package main.java.spells.abstractions;

public enum SpellType {
    ArcaneMissile,
    Barrier,
    ChaosBolt,
    Regrowth,
    SwiftMend,
    Rapture
}
