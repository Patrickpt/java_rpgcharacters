package main.java.spells.abstractions;

public enum SpellCategory {
    Heal,
    Shield,
    Damage
}
