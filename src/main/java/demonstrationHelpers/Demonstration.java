package main.java.demonstrationHelpers;

import main.java.characters.*;
import main.java.characters.abstractions.Hero;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.Cloth;
import main.java.items.armor.Leather;
import main.java.items.armor.Mail;
import main.java.items.armor.Plate;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.*;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.*;
import main.java.spells.abstractions.DamageType;

import java.util.ArrayList;

public class Demonstration {
    ArrayList<Hero> partylist;
    ArmorFactory armorFactory = new ArmorFactory();
    WeaponFactory weaponFactory = new WeaponFactory();
    public Demonstration(ArrayList<Hero> partyList) {
        this.partylist = partyList;
    }

    public void givePartyWeapon(ItemRarity rarity){
        double rarityModifier;
        switch (rarity){
            case Common:
                rarityModifier = new Common().powerModifier();
                break;
            case Rare:
                rarityModifier = new Rare().powerModifier();
                break;
            case Uncommon:
                rarityModifier = new Uncommon().powerModifier();
                break;
            case Epic:
                rarityModifier = new Epic().powerModifier();
                break;
            case Legendary:
                rarityModifier = new Legendary().powerModifier();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + rarity);
        }
        for(int i = 0; i < partylist.size(); i++){
            switch (partylist.get(i).getClass().getSimpleName()){
                case "Mage":
                    Mage mage = (Mage) partylist.get(i);
                    mage.equipWeapon((MagicWeapon) weaponFactory.getItem(WeaponType.Wand, rarityModifier));
                    System.out.println("Mage equipped " + rarity + " weapon");
                    break;
                case "Druid":
                    Druid druid = (Druid) partylist.get(i);
                    druid.equipWeapon((MagicWeapon) weaponFactory.getItem(WeaponType.Staff, rarityModifier));
                    System.out.println("Druid equipped " + rarity + " weapon");
                    break;
                case "Paladin":
                    Paladin paladin = (Paladin) partylist.get(i);
                    paladin.equipWeapon((BluntWeapon) weaponFactory.getItem(WeaponType.Hammer, rarityModifier));
                    System.out.println("Paladin equipped " + rarity + " weapon");
                    break;
                case "Priest":
                    Priest priest = (Priest) partylist.get(i);
                    priest.equipWeapon((MagicWeapon) weaponFactory.getItem(WeaponType.Staff, rarityModifier));
                    System.out.println("Priest equipped " + rarity + " weapon");
                    break;
                case "Ranger":
                    Ranger ranger = (Ranger) partylist.get(i);
                    ranger.equipWeapon((RangedWeapon) weaponFactory.getItem(WeaponType.Bow, rarityModifier));
                    System.out.println("Ranger equipped " + rarity + " weapon");
                    break;
                case "Rogue":
                    Rogue rogue = (Rogue) partylist.get(i);
                    rogue.equipWeapon((BladedWeapon) weaponFactory.getItem(WeaponType.Dagger, rarityModifier));
                    System.out.println("Rogue equipped " + rarity + " weapon");
                    break;
                case "Warlock":
                    Warlock warlock = (Warlock) partylist.get(i);
                    warlock.equipWeapon((MagicWeapon) weaponFactory.getItem(WeaponType.Wand, rarityModifier));
                    System.out.println("Warlock equipped " + rarity + " weapon");
                    break;
                case "Warrior":
                    Warrior warrior = (Warrior) partylist.get(i);
                    warrior.equipWeapon((BladedWeapon) weaponFactory.getItem(WeaponType.Sword, rarityModifier));
                    System.out.println("Warrior equipped " + rarity + " weapon");
                    break;
            }
        }

    }

    public void givePartyArmor(ItemRarity rarity){
        double rarityModifier;
        switch (rarity){
            case Common:
                rarityModifier = new Common().powerModifier();
                break;
            case Rare:
                rarityModifier = new Rare().powerModifier();
                break;
            case Uncommon:
                rarityModifier = new Uncommon().powerModifier();
                break;
            case Epic:
                rarityModifier = new Epic().powerModifier();
                break;
            case Legendary:
                rarityModifier = new Legendary().powerModifier();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + rarity);
        }
        for(int i = 0; i < partylist.size(); i++){
            switch (partylist.get(i).getClass().getSimpleName()){
                case "Mage":
                    Mage mage = (Mage) partylist.get(i);
                    mage.equipArmor((Cloth) armorFactory.getArmor(ArmorType.Cloth, rarityModifier));
                    System.out.println("Mage equipped " + rarity + " armor");
                    break;
                case "Druid":
                    Druid druid = (Druid) partylist.get(i);
                    druid.equipArmor((Leather) armorFactory.getArmor(ArmorType.Leather, rarityModifier));
                    System.out.println("Druid equipped " + rarity + " armor");
                    break;
                case "Paladin":
                    Paladin paladin = (Paladin) partylist.get(i);
                    paladin.equipArmor((Plate) armorFactory.getArmor(ArmorType.Plate, rarityModifier));
                    System.out.println("Paladin equipped " + rarity + " armor");
                    break;
                case "Priest":
                    Priest priest = (Priest) partylist.get(i);
                    priest.equipArmor((Cloth) armorFactory.getArmor(ArmorType.Cloth, rarityModifier));
                    System.out.println("Priest equipped " + rarity + " armor");
                    break;
                case "Ranger":
                    Ranger ranger = (Ranger) partylist.get(i);
                    ranger.equipArmor((Mail) armorFactory.getArmor(ArmorType.Mail, rarityModifier));
                    System.out.println("Ranger equipped " + rarity + " armor");
                    break;
                case "Rogue":
                    Rogue rogue = (Rogue) partylist.get(i);
                    rogue.equipArmor((Leather) armorFactory.getArmor(ArmorType.Leather, rarityModifier));
                    System.out.println("Rogue equipped " + rarity + " armor");
                    break;
                case "Warlock":
                    Warlock warlock = (Warlock) partylist.get(i);
                    warlock.equipArmor((Cloth) armorFactory.getArmor(ArmorType.Cloth, rarityModifier));
                    System.out.println("Warlock equipped " + rarity + " armor");
                    break;
                case "Warrior":
                    Warrior warrior = (Warrior) partylist.get(i);
                    warrior.equipArmor((Plate) armorFactory.getArmor(ArmorType.Plate, rarityModifier));
                    System.out.println("Warrior equipped " + rarity + " armor");
                    break;
            }
        }
    }

    public void performActions(){
        //Loop through party and perform the action they can do (attack, heal or shield)
        for(int i = 0; i < partylist.size(); i++){
            switch (partylist.get(i).getClass().getSimpleName()){
                case "Mage":
                    Mage mage = (Mage) partylist.get(i);
                    System.out.println("Mage casting spell! Dealt " + mage.castDamagingSpell() + " damage");
                    break;
                case "Druid":
                    Druid druid = (Druid) partylist.get(i);
                    System.out.println("Druid is healing! Healed " + druid.healPartyMember() + " health");
                    break;
                case "Paladin":
                    Paladin paladin = (Paladin) partylist.get(i);
                    System.out.println("Paladin is attacking! Dealt " + paladin.attackWithBluntWeapon() + " damage");
                    break;
                case "Priest":
                    Priest priest = (Priest) partylist.get(i);
                    System.out.println("Priest is shielding a partymember with 300 max health! shielded " + priest.shieldPartyMember(300) + " damage");
                    break;
                case "Ranger":
                    Ranger ranger = (Ranger) partylist.get(i);
                    System.out.println("Ranger is attacking! Dealt " + ranger.attackWithRangedWeapon() + " damage");
                    break;
                case "Rogue":
                    Rogue rogue = (Rogue) partylist.get(i);
                    System.out.println("Rogue is attacking! Dealt " + rogue.attackWithBladedWeapon() + " damage");
                    break;
                case "Warlock":
                    Warlock warlock = (Warlock) partylist.get(i);
                    System.out.println("Warlock is casting spell! Dealt " + warlock.castDamagingSpell() + " damage");
                    break;
                case "Warrior":
                    Warrior warrior = (Warrior) partylist.get(i);
                    System.out.println("Warrior is attacking! Dealt " + warrior.attackWithBladedWeapon() + " damage");
                    break;
            }
        }
    }

    public void takeDamage(){
        for(int i = 0; i < partylist.size(); i++){
            System.out.println(partylist.get(i).getClass().getSimpleName() + " is taking physical damage! Took " + partylist.get(i).takeDamage(300, DamageType.Physical) + " damage");
            System.out.println(partylist.get(i).getClass().getSimpleName() + " is taking magic damage! Took " + partylist.get(i).takeDamage(300, DamageType.Magic) + " damage");
            System.out.println("\n");
        }
    }


    public ArrayList<Hero> getPartylist() {
        return partylist;
    }
}
