package main.java.items.weapons.blunt;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BluntWeapon;

public class Hammer implements BluntWeapon {
    // Rarity
    private double rarity;

    // Public properties
    public double getRarity() {
        return rarity;
    }

    public Hammer(double rarity) {
        this.rarity = rarity;
    }

    @Override
    public double getAttackPowerModifier() {
        return WeaponStatsModifiers.HAMMER_ATTACK_MOD;
    }
}
