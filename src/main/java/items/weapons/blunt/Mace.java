package main.java.items.weapons.blunt;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BluntWeapon;

public class Mace implements BluntWeapon {
    // Rarity
    private double rarity;

    // Public properties
    public double getRarity() {
        return rarity;
    }

    public Mace(double rarity) {
        this.rarity = rarity;
    }

    @Override
    public double getAttackPowerModifier() {
        return WeaponStatsModifiers.MACE_ATTACK_MOD;
    }
}
