package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.RangedWeapon;

public class Gun implements RangedWeapon {
    // Rarity
    private double rarity;

    // Public properties
    public double getRarity() {
        return rarity;
    }

    public Gun(double rarity) {
        this.rarity = rarity;
    }

    @Override
    public double getAttackPowerModifier() {
        return WeaponStatsModifiers.GUN_ATTACK_MOD;
    }
}
