package main.java.items.weapons.bladed;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BladedWeapon;

public class Sword implements BladedWeapon {
    // Rarity
    private double rarity;

    // Public properties
    public double getRarity() {
        return rarity;
    }

    public Sword(double rarity) {
        this.rarity = rarity;
    }

    @Override
    public double getAttackPowerModifier() {
        return WeaponStatsModifiers.SWORD_ATTACK_MOD;
    }
}
