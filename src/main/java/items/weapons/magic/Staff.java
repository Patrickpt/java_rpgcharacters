package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.MagicWeapon;

public class Staff implements MagicWeapon {
    // Rarity
    private double rarity;

    // Public properties
    public double getRarity() {
        return rarity;
    }


    public Staff(double itemRarity) {
        this.rarity = itemRarity;
    }

    @Override
    public double getMagicPowerModifier() {
        return WeaponStatsModifiers.STAFF_MAGIC_MOD;
    }
}
