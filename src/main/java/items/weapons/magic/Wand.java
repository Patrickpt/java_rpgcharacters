package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.MagicWeapon;

public class Wand implements MagicWeapon {
    // Rarity
    private double rarity;

    // Public properties
    public double getRarity() {
        return rarity;
    }
    public double getMagicPowerModifier(){
        return WeaponStatsModifiers.WAND_MAGIC_MOD;
    }

    public Wand(double rarity) {
        this.rarity = rarity;
    }
}
