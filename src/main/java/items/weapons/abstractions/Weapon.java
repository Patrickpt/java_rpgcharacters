package main.java.items.weapons.abstractions;

import main.java.items.rarity.abstractions.ItemRarity;

public interface Weapon {
    double getRarity();
}
