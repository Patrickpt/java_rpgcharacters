package main.java.items.weapons.abstractions;

public interface MagicWeapon extends Weapon{
    double getMagicPowerModifier();
}
