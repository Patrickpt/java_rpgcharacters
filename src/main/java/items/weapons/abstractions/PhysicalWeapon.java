package main.java.items.weapons.abstractions;

public interface PhysicalWeapon extends Weapon {
    double getAttackPowerModifier();
}
