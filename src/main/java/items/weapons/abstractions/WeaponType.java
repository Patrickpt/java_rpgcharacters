package main.java.items.weapons.abstractions;

public enum WeaponType {
    Axe,
    Bow,
    Crossbow,
    Dagger,
    Gun,
    Hammer,
    Mace,
    Staff,
    Sword,
    Wand
}
