package main.java.items.rarity.abstractions;

public enum ItemRarity {
    Common,
    Uncommon,
    Rare,
    Epic,
    Legendary
}
