package main.java.items.armor.abstractions;

public interface Armor {
    // Public properties
    double getHealthModifier();

    double getPhysRedModifier();

    double getMagicRedModifier();

    double getRarityModifier();
}
