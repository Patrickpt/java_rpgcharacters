package main.java.characters;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Hero;
import main.java.characters.abstractions.categories.BladeAttacker;
import main.java.characters.abstractions.wearers.LeatherWearer;
import main.java.characters.abstractions.wielders.BladeWielder;
import main.java.items.armor.Leather;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.BladedWeapon;
import main.java.items.weapons.bladed.Dagger;

/*
 Rogues are stealthy combatants of the shadows.
 They wield bladed weapons with great agility and dispatch their enemies swiftly.
*/
public class Rogue extends Hero implements LeatherWearer, BladeWielder, BladeAttacker {
    BladedWeapon bladedWeapon;

    // Base stats defensive
    private static double baseHealth = CharacterBaseStatsDefensive.ROGUE_BASE_HEALTH;
    private static double basePhysReductionPercent = CharacterBaseStatsDefensive.ROGUE_BASE_PHYS_RED; // Armor
    private static double baseMagicReductionPercent = CharacterBaseStatsDefensive.ROGUE_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.ROGUE_MELEE_ATTACK_POWER;

    // Constructor
    public Rogue() {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, new Leather(new Common().powerModifier()), new Dagger(new Common().powerModifier()));
        bladedWeapon = (BladedWeapon) super.getWeapon();
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Leather armor) {
        super.setArmor(armor);
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(BladedWeapon weapon) {
        super.setWeapon(weapon);
    }

    // Character behaviours

    /**
     * Damages the enemy
     */
    public double attackWithBladedWeapon() {
        bladedWeapon = (BladedWeapon) super.getWeapon();
        double damage = baseAttackPower*bladedWeapon.getAttackPowerModifier()*bladedWeapon.getRarity();
        return damage;
    }
}
