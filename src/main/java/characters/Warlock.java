package main.java.characters;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Hero;
import main.java.characters.abstractions.categories.SpellCaster;
import main.java.characters.abstractions.wearers.ClothWearer;
import main.java.characters.abstractions.wielders.MagicWielder;
import main.java.items.armor.Cloth;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.magic.Wand;
import main.java.spells.abstractions.DamagingSpell;
/*
 Warlocks are masters of chaos, entropy, and death. They were once mages but were corrupted by power.
 They can conjure up pure chaos energy to destroy their enemies.
*/
public class Warlock extends Hero implements ClothWearer, MagicWielder, SpellCaster {
    private DamagingSpell damagingSpell;
    private MagicWeapon magicWeapon;

    // Base stats defensive
    private static double baseHealth = CharacterBaseStatsDefensive.WARLOCK_BASE_HEALTH;
    private static double basePhysReductionPercent = CharacterBaseStatsDefensive.WARLOCK_BASE_PHYS_RED; // Armor
    private static double baseMagicReductionPercent = CharacterBaseStatsDefensive.WARLOCK_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseMagicPower = CharacterBaseStatsOffensive.WARLOCK_MAGIC_POWER;

    // Constructor
    public Warlock() {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, new Cloth(new Common().powerModifier()), new Wand(new Common().powerModifier()));
    }

    public Warlock(DamagingSpell damagingSpell) {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, new Cloth(new Common().powerModifier()), new Wand(new Common().powerModifier()));
        this.damagingSpell = damagingSpell;
        this.magicWeapon = (MagicWeapon)super.getWeapon();
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Cloth armor) {
        super.setArmor(armor);
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(MagicWeapon weapon) {
        super.setWeapon(weapon);
    }

    // Character behaviours

    /**
     * Damages the enemy with its spells
     */
    public double castDamagingSpell() {
        magicWeapon = (MagicWeapon) super.getWeapon();
        double damage = baseMagicPower*magicWeapon.getMagicPowerModifier()*damagingSpell.getSpellDamageModifier()*magicWeapon.getRarity();
        return damage;
    }
}
