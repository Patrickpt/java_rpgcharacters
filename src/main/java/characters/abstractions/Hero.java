package main.java.characters.abstractions;

import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;
import main.java.spells.abstractions.DamageType;

public abstract class Hero {
    private double baseHealth;
    private double basePhysReductionPercent;
    private double baseMagicReductionPercent;
    private Boolean isDead;
    private double currentHealth;
    private Armor armor;
    private Weapon weapon;

    public Hero(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, Armor armor, Weapon weapon){
        this.baseHealth = baseHealth;
        this.basePhysReductionPercent = basePhysReductionPercent;
        this.baseMagicReductionPercent = baseMagicReductionPercent;
        this.armor = armor;
        this.weapon = weapon;
        this.isDead = false;
        this.currentHealth = baseHealth;
    }

    public double getCurrentMaxHealth(){
        return baseHealth*armor.getHealthModifier()*armor.getRarityModifier();
    }

    public double getCurrentHealth(){
        return currentHealth*armor.getHealthModifier()*armor.getRarityModifier();
    }

    public Boolean getDead(){
        return isDead;
    }

    protected void setArmor(Armor armor){
        this.armor = armor;
    }

    public Armor getArmor(){
        return this.armor;
    }

    protected void setWeapon(Weapon weapon){
        this.weapon = weapon;
    }

    public Weapon getWeapon(){
        return this.weapon;
    }

    public double takeDamage(double incomingDamage, DamageType damageType){
        System.out.println("Incoming damage: " + incomingDamage);
        switch(damageType){
            case Physical:
                double physDamage = incomingDamage*(1 - (basePhysReductionPercent*armor.getPhysRedModifier()*armor.getRarityModifier()));
                return physDamage;
            case Magic:
                double magicDamage = incomingDamage*(1 - (baseMagicReductionPercent*armor.getMagicRedModifier()*armor.getRarityModifier()));
                return magicDamage;
            default:
                throw new IllegalArgumentException("Invalid damagetype");
        }
    }

    @Override
    public String toString() {
        return "Hero{" +
                "baseHealth=" + baseHealth +
                ", basePhysReductionPercent=" + basePhysReductionPercent +
                ", baseMagicReductionPercent=" + baseMagicReductionPercent +
                ", isDead=" + isDead +
                ", currentHealth=" + currentHealth +
                ", armor=" + armor +
                ", weapon=" + weapon +
                '}';
    }
}