package main.java.characters.abstractions.wearers;

import main.java.items.armor.Leather;

public interface LeatherWearer {
    void equipArmor(Leather armor);
}
