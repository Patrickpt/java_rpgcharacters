package main.java.characters.abstractions.wearers;

import main.java.items.armor.Plate;

public interface PlateWearer {
    void equipArmor(Plate armor);
}
