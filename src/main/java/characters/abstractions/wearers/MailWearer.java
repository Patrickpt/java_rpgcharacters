package main.java.characters.abstractions.wearers;

import main.java.items.armor.Mail;

public interface MailWearer {
    void equipArmor(Mail armor);
}
