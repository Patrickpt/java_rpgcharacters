package main.java.characters.abstractions.wearers;

import main.java.items.armor.Cloth;

public interface ClothWearer {
    void equipArmor(Cloth armor);
}
