package main.java.characters.abstractions.categories;

public interface SpellCaster {
    double castDamagingSpell();
}
