package main.java.characters.abstractions.categories;

public interface Shielder {
    double shieldPartyMember(double partyMemberMaxHealth);
}
