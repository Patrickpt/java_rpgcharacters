package main.java.characters.abstractions.categories;

public interface Healer {
    double healPartyMember();
}
