package main.java.characters.abstractions.categories;

public interface BluntAttacker {
    double attackWithBluntWeapon();
}
