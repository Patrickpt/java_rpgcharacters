package main.java.characters.abstractions.categories;

public interface BladeAttacker {
    double attackWithBladedWeapon();
}
