package main.java.characters.abstractions.categories;

public interface RangedAttacker {
    double attackWithRangedWeapon();
}
