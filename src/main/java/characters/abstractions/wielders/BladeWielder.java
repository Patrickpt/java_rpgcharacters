package main.java.characters.abstractions.wielders;

import main.java.items.weapons.abstractions.BladedWeapon;

public interface BladeWielder {
    void equipWeapon(BladedWeapon weapon);
}
