package main.java.characters.abstractions.wielders;

import main.java.items.weapons.abstractions.MagicWeapon;

public interface MagicWielder {
    void equipWeapon(MagicWeapon weapon);
}
