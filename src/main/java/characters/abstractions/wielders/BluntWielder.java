package main.java.characters.abstractions.wielders;

import main.java.items.weapons.abstractions.BluntWeapon;

public interface BluntWielder {
    void equipWeapon(BluntWeapon weapon);
}
