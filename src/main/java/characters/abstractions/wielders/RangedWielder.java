package main.java.characters.abstractions.wielders;

import main.java.items.weapons.abstractions.RangedWeapon;

public interface RangedWielder {
    void equipWeapon(RangedWeapon weapon);
}
