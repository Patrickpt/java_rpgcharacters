package main.java.characters;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Hero;
import main.java.characters.abstractions.categories.BluntAttacker;
import main.java.characters.abstractions.wearers.PlateWearer;
import main.java.characters.abstractions.wielders.BluntWielder;
import main.java.items.armor.Plate;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.BluntWeapon;
import main.java.items.weapons.blunt.Hammer;

/*
 Paladins are faithful servants of the light and everything holy.
 They dispatch justice and are filled with vengeance for the wrongs done to society by evil.
 Paladins are very durable and typically wield heavy weapons.
*/
public class Paladin extends Hero implements PlateWearer, BluntWielder, BluntAttacker {
    private BluntWeapon bluntWeapon;

    // Base stats defensive
    private static double baseHealth = CharacterBaseStatsDefensive.PALADIN_BASE_HEALTH;
    private static double basePhysReductionPercent = CharacterBaseStatsDefensive.PALADIN_BASE_PHYS_RED; // Armor
    private static double baseMagicReductionPercent = CharacterBaseStatsDefensive.PALADIN_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.PALADIN_MELEE_ATTACK_POWER;

    // Constructor
    public Paladin() {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, new Plate(new Common().powerModifier()), new Hammer(new Common().powerModifier()));
        bluntWeapon = (BluntWeapon)super.getWeapon();

    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Plate armor) {
        super.setArmor(armor);
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(BluntWeapon weapon) {
        super.setWeapon(weapon);
    }

    // Character behaviours

    /**
     * Damages the enemy
     */
    public double attackWithBluntWeapon() {
        bluntWeapon = (BluntWeapon) super.getWeapon();
        double damage = baseAttackPower*bluntWeapon.getAttackPowerModifier()*bluntWeapon.getRarity();
        return damage;
    }
}
