package main.java.characters;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Hero;
import main.java.characters.abstractions.categories.SpellCaster;
import main.java.characters.abstractions.wearers.ClothWearer;
import main.java.characters.abstractions.wielders.MagicWielder;
import main.java.items.armor.Cloth;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.magic.Wand;
import main.java.spells.abstractions.DamagingSpell;

/*
 Class description:
 ------------------
 Mages are masters of arcane magic. Their skills are honed after years of dedicated study.
 They conjure arcane energy to deal large amounts of damage to enemies.
 They are vulnerable to physical attacks but are resistant to magic.
*/
public class Mage extends Hero implements ClothWearer, MagicWielder, SpellCaster {
    private DamagingSpell damagingSpell;
    private MagicWeapon magicWeapon;

    // Base stats defensive
    private static double baseHealth = CharacterBaseStatsDefensive.MAGE_BASE_HEALTH;
    private static double basePhysReductionPercent = CharacterBaseStatsDefensive.MAGE_BASE_PHYS_RED; // Armor
    private static double baseMagicReductionPercent = CharacterBaseStatsDefensive.MAGE_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseMagicPower = CharacterBaseStatsOffensive.MAGE_MAGIC_POWER;

    // Active trackers and flags

    // Constructor
    public Mage() {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, new Cloth(new Common().powerModifier()), new Wand(new Common().powerModifier()));
        // When the character is created it has maximum health (base health)
    }

    public Mage(DamagingSpell damagingSpell) {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, new Cloth(new Common().powerModifier()), new Wand(new Common().powerModifier()));
        this.damagingSpell = damagingSpell;
        this.magicWeapon = (MagicWeapon)super.getWeapon();
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Cloth armor) {
        super.setArmor(armor);
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(MagicWeapon weapon) {
        super.setWeapon(weapon);
    }

    // Character behaviours

    /**
     * Damages the enemy with its spells
     */
    public double castDamagingSpell() {
        magicWeapon = (MagicWeapon) super.getWeapon();
        double damage = baseMagicPower*magicWeapon.getMagicPowerModifier()*damagingSpell.getSpellDamageModifier()*magicWeapon.getRarity();
        return damage;
    }

    @Override
    public String toString() {
        return "Mage{" +
                "damagingSpell=" + damagingSpell +
                ", magicWeapon=" + magicWeapon +
                ", baseMagicPower=" + baseMagicPower +
                "} " + super.toString();
    }
}
