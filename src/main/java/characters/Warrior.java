package main.java.characters;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Hero;
import main.java.characters.abstractions.categories.BladeAttacker;
import main.java.characters.abstractions.wearers.PlateWearer;
import main.java.characters.abstractions.wielders.BladeWielder;
import main.java.items.armor.Plate;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.BladedWeapon;
import main.java.items.weapons.bladed.Sword;

/*
 Warriors are combat veterans, durable forces on the battlefield.
 They are masters of the blade and wield it with unmatched ferocity.
*/
public class Warrior extends Hero implements PlateWearer, BladeAttacker, BladeWielder {
    BladedWeapon bladedWeapon;

    // Base stats defensive
    private static double baseHealth = CharacterBaseStatsDefensive.WARRIOR_BASE_HEALTH;
    private static double basePhysReductionPercent = CharacterBaseStatsDefensive.WARRIOR_BASE_PHYS_RED; // Armor
    private static double baseMagicReductionPercent = CharacterBaseStatsDefensive.WARRIOR_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.WARRIOR_MELEE_ATTACK_POWER;

    // Constructor
    public Warrior() {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, new Plate(new Common().powerModifier()), new Sword(new Common().powerModifier()));
        bladedWeapon = (BladedWeapon) super.getWeapon();
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Plate armor) {
        super.setArmor(armor);
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(BladedWeapon weapon) {
        super.setWeapon(weapon);
    }

    // Character behaviours

    /**
     * Damages the enemy
     */
    public double attackWithBladedWeapon() {
        bladedWeapon = (BladedWeapon) super.getWeapon();
        double damage = baseAttackPower*bladedWeapon.getAttackPowerModifier()*bladedWeapon.getRarity();
        return damage;
    }
}
