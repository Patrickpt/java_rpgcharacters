package main.java.characters;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Hero;
import main.java.characters.abstractions.categories.RangedAttacker;
import main.java.characters.abstractions.wearers.MailWearer;
import main.java.characters.abstractions.wielders.RangedWielder;
import main.java.items.armor.Mail;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.RangedWeapon;
import main.java.items.weapons.ranged.Bow;

/*
 Rangers
 Are masters of ranged combat. They use a wide arsenal of weapons to dispatch enemies.
*/
public class Ranger extends Hero implements MailWearer, RangedWielder, RangedAttacker {
    private RangedWeapon rangedWeapon;

    // Base stats defensive
    private static double baseHealth = CharacterBaseStatsDefensive.RANGER_BASE_HEALTH;
    private static double basePhysReductionPercent = CharacterBaseStatsDefensive.RANGER_BASE_PHYS_RED; // Armor
    private static double baseMagicReductionPercent = CharacterBaseStatsDefensive.RANGER_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.RANGER_RANGED_ATTACK_POWER;

    // Constructor
    public Ranger() {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, new Mail(new Common().powerModifier()), new Bow(new Common().powerModifier()));
        rangedWeapon = (RangedWeapon)super.getWeapon();
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Mail armor) {
        super.setArmor(armor);
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(RangedWeapon weapon) {
        super.setWeapon(weapon);
    }

    // Character behaviours

    /**
     * Damages the enemy
     */
    public double attackWithRangedWeapon() {
        rangedWeapon = (RangedWeapon) super.getWeapon();
        double damage = baseAttackPower*rangedWeapon.getAttackPowerModifier()*rangedWeapon.getRarity();
        return damage;
    }

}
