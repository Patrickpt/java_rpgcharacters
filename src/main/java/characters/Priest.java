package main.java.characters;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.Hero;
import main.java.characters.abstractions.categories.Shielder;
import main.java.characters.abstractions.wearers.ClothWearer;
import main.java.characters.abstractions.wielders.MagicWielder;
import main.java.items.armor.Cloth;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.magic.Staff;
import main.java.spells.abstractions.ShieldingSpell;

/*
 Priest are the servants of the light and goodness.
 They use holy magic to heal and shield allies.
 As a support class they only have defensive stats.
*/
public class Priest extends Hero implements ClothWearer, MagicWielder, Shielder {
    private ShieldingSpell shieldingSpell;
    private MagicWeapon magicWeapon;

    // Base stats defensive
    private static double baseHealth = CharacterBaseStatsDefensive.PRIEST_BASE_HEALTH;
    private static double basePhysReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_PHYS_RED; // Armor
    private static double baseMagicReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_MAGIC_RES; // Magic armor

    // Constructor
    public Priest() {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, new Cloth(new Common().powerModifier()), new Staff(new Common().powerModifier()));
    }

    public Priest(ShieldingSpell shieldingSpell) {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, new Cloth(new Common().powerModifier()),new Staff(new Common().powerModifier()));
        this.shieldingSpell = shieldingSpell;
        magicWeapon = (MagicWeapon) super.getWeapon();
    }
    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Cloth armor) {
        super.setArmor(armor);
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(MagicWeapon weapon) {
        super.setWeapon(weapon);
    }

    // Character behaviours

    /**
     * Shields a party member for a percentage of their maximum health.
     * @param partyMemberMaxHealth
     */
    public double shieldPartyMember(double partyMemberMaxHealth) {
        magicWeapon = (MagicWeapon) super.getWeapon();
        double shieldSize = (partyMemberMaxHealth*shieldingSpell.getAbsorbShieldPercentage())+(magicWeapon.getMagicPowerModifier()*magicWeapon.getRarity());
        return shieldSize;
    }

}
