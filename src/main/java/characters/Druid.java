package main.java.characters;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.Hero;
import main.java.characters.abstractions.categories.Healer;
import main.java.characters.abstractions.wearers.LeatherWearer;
import main.java.characters.abstractions.wielders.MagicWielder;
import main.java.items.armor.Cloth;
import main.java.items.armor.Leather;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.magic.Staff;
import main.java.spells.abstractions.HealingSpell;

/*
 Class description:
 ------------------
 Druids are spell casters who use nature based magic to aid their allies in battle.
 They can heal their allies or protect them using the forces of nature.
 As a support class they only have defensive stats.
*/
public class Druid extends Hero implements LeatherWearer, MagicWielder, Healer {
    private HealingSpell healingSpell;
    private MagicWeapon magicWeapon;

    // Base stats defensive
    private static double baseHealth = CharacterBaseStatsDefensive.DRUID_BASE_HEALTH;
    private static double basePhysReductionPercent = CharacterBaseStatsDefensive.DRUID_BASE_PHYS_RED; // Armor
    private static double baseMagicReductionPercent = CharacterBaseStatsDefensive.DRUID_BASE_MAGIC_RES; // Magic armor

    // Constructor
    public Druid() {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, new Cloth(new Common().powerModifier()), new Staff(new Common().powerModifier()));
    }

    public Druid(HealingSpell healingSpell) {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, new Cloth(new Common().powerModifier()), new Staff(new Common().powerModifier()));
        this.healingSpell = healingSpell;
        this.magicWeapon = (MagicWeapon) super.getWeapon();
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Leather armor) {
        super.setArmor(armor);
    }

    /**
     * Equips a weapon to the character, modifying stats
     * @param weapon
     */
    public void equipWeapon(MagicWeapon weapon) {
        super.setWeapon(weapon);
    }

    // Character behaviours

    /**
     * Heals the party member
     */
    public double healPartyMember() {
        magicWeapon = (MagicWeapon) super.getWeapon();
        double healing = healingSpell.getHealingAmount()*magicWeapon.getMagicPowerModifier()*magicWeapon.getRarity();
        return healing;
    }
}
