package main.java.factories;
// Imports

import main.java.characters.*;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.Hero;
import main.java.spells.damaging.ArcaneMissile;
import main.java.spells.damaging.ChaosBolt;
import main.java.spells.healing.Regrowth;
import main.java.spells.shielding.Barrier;

/*
 This factory exists to be responsible for creating new enemies.
 Object is replaced with Character as a return type when refactored to be good OO design.
*/
// TODO Once characters can be made with the right compositions, then implement the factory
public class CharacterFactory {
    public Hero getCharacter(CharacterCategory characterType) {
        switch(characterType) {
            case Mage:
                return new Mage(new ArcaneMissile());
            case Warlock:
                return new Warlock(new ChaosBolt());
            case Druid:
                return new Druid(new Regrowth());
            case Priest:
                return new Priest(new Barrier());
            case Paladin:
                return new Paladin();
            case Ranger:
                return new Ranger();
            case Rogue:
                return new Rogue();
            case Warrior:
                return new Warrior();
            default:
                return null;
        }
    }
}
