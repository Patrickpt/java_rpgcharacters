package main.java.factories;
// Imports

import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.items.weapons.bladed.Axe;
import main.java.items.weapons.bladed.Dagger;
import main.java.items.weapons.bladed.Sword;
import main.java.items.weapons.blunt.Hammer;
import main.java.items.weapons.blunt.Mace;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.magic.Wand;
import main.java.items.weapons.ranged.Bow;
import main.java.items.weapons.ranged.Crossbow;
import main.java.items.weapons.ranged.Gun;
/*
 This factory exists to be responsible for creating new enemies.
 Object is replaced with Weapon as a return type when refactored to be good OO design.
*/
public class WeaponFactory {
    public Weapon getItem(WeaponType weaponType, double itemRarity) {
        switch(weaponType) {
            case Axe:
                return new Axe(itemRarity);
            case Bow:
                return new Bow(itemRarity);
            case Crossbow:
                return new Crossbow(itemRarity);
            case Dagger:
                return new Dagger(itemRarity);
            case Gun:
                return new Gun(itemRarity);
            case Hammer:
                return new Hammer(itemRarity);
            case Mace:
                return new Mace(itemRarity);
            case Staff:
                return new Staff(itemRarity);
            case Sword:
                return new Sword(itemRarity);
            case Wand:
                return new Wand(itemRarity);
            default:
                return null;
        }
    }
}
