package main.java;

import main.java.characters.Druid;
import main.java.characters.Mage;
import main.java.characters.Paladin;
import main.java.characters.Ranger;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.Hero;
import main.java.demonstrationHelpers.Demonstration;
import main.java.factories.CharacterFactory;
import main.java.items.rarity.abstractions.ItemRarity;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        CharacterFactory characterFactory = new CharacterFactory();

        // Create party
        ArrayList<Hero> party = new ArrayList<Hero>();
        Mage mage = (Mage) characterFactory.getCharacter(CharacterCategory.Mage);
        Druid druid = (Druid) characterFactory.getCharacter(CharacterCategory.Druid);
        Paladin paladin = (Paladin) characterFactory.getCharacter(CharacterCategory.Paladin);
        Ranger ranger = (Ranger) characterFactory.getCharacter(CharacterCategory.Ranger);
        party.add(mage);
        party.add(druid);
        party.add(paladin);
        party.add(ranger);
        Demonstration demonstration = new Demonstration(party);

        //Demonstrate abilities with default equipment
        System.out.println("----- Attacking and healing with default equipment -----");
        demonstration.performActions();
        System.out.println("\n");

        //Give the party legendary weapons and demonstrate abilities again
        demonstration.givePartyWeapon(ItemRarity.Legendary);
        System.out.println("\n ----- Attacking and healing after equipping legendary weapons -----");
        demonstration.performActions();

        //Demonstrate taking damage with default armor
        System.out.println("\n ----- Taking physical and magic damage with default armor -----");
        demonstration.takeDamage();
        System.out.println("\n");

        //Give party legendary armor and demonstrate taking damage again
        demonstration.givePartyArmor(ItemRarity.Legendary);
        System.out.println("\n ----- Taking physical and magic damage with legendary armor -----");
        demonstration.takeDamage();
    }
}
