# RPG Character generation
The focus of this project was refactoring the code to follow good Object Oriented design.
There was also implemented logic for attacking/healing/shielding, equipping weapons and equipping armor for every character.
Running the program gives a simple demonstration
of the logic.